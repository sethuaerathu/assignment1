const express = require ('express');
const path = require ('path');
const {check, validationResult} = require('express-validator');

//DB Connection
const mongoose = require('mongoose');
mongoose.connect('mongodb://localhost:27017/onlineclothshop ',{
    useNewUrlParser : true,
    useUnifiedTopology : true
});



//Model for the order
const Order = mongoose.model('Order', {
    name : String,
    email : String,
    phone : String,
    address : String,
    city : String,
    province : String,
    saree : Number,
    sareeTotal : Number,
    jeans : Number,
    jeansTotal : Number,
    top : Number,
    topTotal : Number,
    churidhar : Number,
    churidharTotal : Number,
    shirt : Number,
    shirtTotal : Number,
    subTotal : Number,
    tax : Number,
    taxPer : Number,
    total : Number    
})

var myApp = express ();
myApp.use (express. urlencoded({extended:true}));


myApp.set ('views', path.join (__dirname, 'views'));
myApp.use (express.static(__dirname + '/public'));
myApp.use( express.static( "public" ));
myApp.set ('view engine', 'ejs');


//home page 
myApp.get ('/', function (req, res)
{
    res.render('form'); 
});

myApp.post('/', [
    check ('name', 'Name is required!').notEmpty(),
    check ('address', 'Address is required!').notEmpty(),
    check ('city', 'City is required!').notEmpty(),
    check ('email', 'Email is required!').isEmail(),
    check ('phone', 'Phone is required!').isMobilePhone(),
    check ('province', 'Province is required!').notEmpty()
], function (req,res){
    const errors = validationResult(req);
    console.log(errors);
    if (!errors.isEmpty())
    {
        res.render('form', {
            errors : errors.array()
        });
    }
    else 
    {
        var name = req.body.name;
        var email = req.body.email;
        var phone = req.body.phone;
        var address = req.body.address;
        var city = req.body.city;
        var province = req.body.province;
        var saree = req.body.saree;
        var jeans = req.body.jeans;
        var top = req.body.top;
        var churidhar = req.body.churidhar;
        var shirt = req.body.shirt;

        var sareePrice = 150 , jeansPrice = 45, topPrice = 35, churidharPrice = 100,shirtPrice =70,subTotal = 0, displayReceipt;
        if( saree> 0){
            var sareeTotal = sareePrice * saree;
            subTotal += sareeTotal;
        }
        if(jeans > 0){
            var jeansTotal = jeansPrice * jeans;
            subTotal += jeansTotal;
        }
        if(top > 0){
            var topTotal = topPrice * top;
            subTotal += topTotal;
        }
        if(churidhar > 0){
            var churidharTotal = churidharPrice * churidhar;
            subTotal += churidharTotal;
        }
        if(shirt > 0){
            var shirtTotal = shirtPrice * shirt;
            subTotal += shirtTotal;
        }
        
        var tax = 0,taxPer = 0;
        if(province == 'Ontario'){
            taxPer = 0.13;
            tax = subTotal * taxPer;
        }else if(province == 'Quebec'){
            taxPer = 0.9;
            tax = subTotal * taxPer;
        }else if(province == 'Alberta'){
            taxPer = 0.5;
            tax = subTotal * taxPer;
        }
        var total = subTotal + tax;

        if(subTotal < 10){
            displayReceipt = 0;
            var receiptMessage = 'Minimum purchase should be of $10.';
            console.log(displayReceipt + ' ' + receiptMessage);
        }

        var pageData = {
            name : name,
            email : email,
            phone : phone,
            address : address,
            city : city,
            province : province,
            saree : saree,
            sareeTotal : sareeTotal,
            jeans : jeans,
            jeansTotal : jeansTotal,
            top : top,
            topTotal : topTotal,
            churidhar : churidhar,
            churidharTotal : churidharTotal,
            shirt : shirt,
            shirtTotal : shirtTotal,
            subTotal : subTotal,
            tax : tax,
            taxPer : taxPer,
            total : total,
            displayReceipt : displayReceipt,
            receiptMessage : receiptMessage
        }
        var myOrder = new Order(pageData);

        myOrder.save().then(function(){
            console.log("New order created!");
        });
        res.render ('form', pageData);
    }
});


myApp.get('/author', function (req,res){
    res.render('author',{
        name : "Sethulekshmi Aerathu Vinayakumar",
        studentNumber : '8776201'
    });
});

//All Orders Page
myApp.get('/allorders', function(req,res){
    Order.find({}).exec(function (err, orders){
        console.log (err);
        res.render('allorders', {orders : orders});
    });
});

myApp.listen(8090); 
console.log('Website opened at port 8090!');